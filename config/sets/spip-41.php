<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use Rector\Renaming\Rector\FuncCall\RenameFunctionRector;
use SpipLeague\Component\Rector\CodeQuality\LangFileWithReturn\LangFileWithReturnRector;

return RectorConfig::configure()
    ->withConfiguredRule(RenameFunctionRector::class, [
        'generer_url_entite' => 'generer_objet_url',
        'generer_url_entite_absolue' => 'generer_objet_url_absolue',
        'generer_info_entite' => 'generer_objet_info',
        'generer_introduction_entite' => 'generer_objet_introduction',
        'generer_lien_entite' => 'generer_objet_lien',
        'http_status' => 'http_response_code',
    ])
    ->withRules([LangFileWithReturnRector::class])
;
