<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use Rector\Set\ValueObject\LevelSetList;
use SpipLeague\Component\Rector\Set\SpipLevelSetList;
use SpipLeague\Component\Rector\Set\SpipSetList;

return RectorConfig::configure()
    ->withSets([SpipSetList::SPIP_50, LevelSetList::UP_TO_PHP_82, SpipLevelSetList::UP_TO_SPIP_44])
;
