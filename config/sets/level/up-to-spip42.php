<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use SpipLeague\Component\Rector\Set\SpipLevelSetList;
use SpipLeague\Component\Rector\Set\SpipSetList;

return RectorConfig::configure()
    ->withSets([SpipSetList::SPIP_42, SpipLevelSetList::UP_TO_SPIP_41])
;
