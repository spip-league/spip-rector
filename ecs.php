<?php

declare(strict_types=1);

use Symplify\EasyCodingStandard\Config\ECSConfig;

return ECSConfig::configure()
    ->withPreparedSets(psr12: true, common: true, cleanCode: true, symplify: true)
    ->withPaths([
        __DIR__ . '/config',
        __DIR__ . '/rules-tests',
        __DIR__ . '/rules',
        __DIR__ . '/src',
        __DIR__ . '/tests',
    ])
    ->withRootFiles()
    ->withSkip(['*/Source/*', '*/Fixture/*']);
