<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use SpipLeague\Component\Rector\CodeQuality\LangFileWithReturn\LangFileWithReturnRector;

return RectorConfig::configure()
    ->withRules([LangFileWithReturnRector::class])
;
