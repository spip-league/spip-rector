<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use SpipLeague\Component\Rector\CodeQuality\RemoveHeaderConstCheck\RemoveHeaderConstCheckRector;

return RectorConfig::configure()
    ->withRules([RemoveHeaderConstCheckRector::class])
;
