<?php

declare(strict_types=1);

namespace SpipLeague\Component\Rector\Set;

/**
 * @api
 */
final class SpipLevelSetList
{
    /**
     * @var string
     */
    public const UP_TO_SPIP_41 = __DIR__ . '/../../config/sets/level/up-to-spip41.php';

    /**
     * @var string
     */
    public const UP_TO_SPIP_42 = __DIR__ . '/../../config/sets/level/up-to-spip42.php';

    /**
     * @var string
     */
    public const UP_TO_SPIP_43 = __DIR__ . '/../../config/sets/level/up-to-spip43.php';

    /**
     * @var string
     */
    public const UP_TO_SPIP_44 = __DIR__ . '/../../config/sets/level/up-to-spip44.php';

    /**
     * @var string
     */
    public const UP_TO_SPIP_50 = __DIR__ . '/../../config/sets/level/up-to-spip50.php';
}
