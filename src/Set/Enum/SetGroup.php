<?php

declare(strict_types=1);

namespace SpipLeague\Component\Rector\Set\Enum;

final class SetGroup
{
    /**
     * @var string
     */
    public const SPIP = 'spip';
}
