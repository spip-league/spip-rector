<?php

declare(strict_types=1);

namespace SpipLeague\Component\Rector\Set;

/**
 * @api
 */
final class SpipSetList
{
    /**
     * @var string
     */
    public const SPIP_41 = __DIR__ . '/../../config/sets/spip-41.php';

    /**
     * @var string
     */
    public const SPIP_42 = __DIR__ . '/../../config/sets/spip-42.php';

    /**
     * @var string
     */
    public const SPIP_43 = __DIR__ . '/../../config/sets/spip-43.php';

    /**
     * @var string
     */
    public const SPIP_44 = __DIR__ . '/../../config/sets/spip-44.php';

    /**
     * @var string
     */
    public const SPIP_50 = __DIR__ . '/../../config/sets/spip-50.php';
}
